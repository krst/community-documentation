---
title: Recommendations on Team Password Managers
keywords: password managers, passwords, team password managers, authentication
last_updated: October 8, 2018
tags: [account_security, articles]
summary: "A client has asked how best to store sensitive passwords that need to be accessed by more than one person."
sidebar: mydoc_sidebar
permalink: 295-Password_managers.html
folder: mydoc
conf: Public
lang: en
---


# Recommendations on Team Password Managers
## Offline and online password managers for individuals and organizations

### Problem

Although we generally recommend not to store passwords in the cloud and the password manager we usually suggest to use is KeepassXC, which is open source, maintained, and under the user's complete control, when passwords need to be shared with several people KeePassXC may not be the easiest tool to use.


* * *


### Solution

When passwords need to be shared with several people, we can suggest the client a strategy to share KeePassXC databases with their team, as well as recommend a self-hosted password manager or end-to-end encrypted online password managers.

Whatever the client's choice, we should stress that the master passphrase to unblock the password manager needs to be strong and unique. If necessary, we can share with them [this guide for creating strong passwords](https://ssd.eff.org/en/module/creating-strong-passwords).


#### KeePassXC

[KeePassXC](https://keepassxc.org/) is a free and open source cross-platform password manager. It is the tool of choice for individuals, but can be harder to use for sharing passwords in a team. To do so, a database for the team needs to be created and stored online, and the password for unblocking it needs to be shared through a secure channel with the team.

We can send our clients the following instructions for team password management with KeePassXC:

- Every staff member will need to install KeePassXC in their PC.

    KeePassXC can be downloaded from this [page](https://keepassxc.org/download).
    
- You will need to create a database, protected by a strong and unique passphrase, for your team. Individual staff members should also create a separate database for their private keys.

- The shared team database will be stored in your cloud/secure server.

- The passphrase to unblock the team database will be shared in person or through secure encrypted communication channels and stored in the individual staff members' private password databases.

- This solution is free, but it does not allow to share a password with people who are not in the same team.

**Pros:** you have complete control of your shared password database, because it is stored in your server, and the learning curve is not very high, i.e. you just need to be trained to use it, but don't need an IT person to manage it.

**Cons:** If there are more subteams, staff members will have to store a different passphrase for each database they have access to, and they have to share some of these passphrases with other members of their subteam - this is prone to human errors. Passwords cannot be shared easily between staff members who don't belong to the same subteam.


#### Bitwarden

[Bitwarden](https://bitwarden.com) is a freemium open-source cloud-based password management service. It offers paid solutions with features for small groups and organizations and a free solution for individuals. It can also be self-hosted. Bitwarden offers end-to-end encryption using AES 256 bit encryption as well as PBKDF2.

- [More details on Bitwarden's encryption](https://help.bitwarden.com/article/what-encryption-is-used/)
- [Instructions for installing and deploying a self-hosted instance of Bitwarden](https://help.bitwarden.com/article/install-on-premise/)
- [Code on Github](https://github.com/bitwarden)


#### LastPass

[LastPass](https://www.lastpass.com/) is an online password manager. It is end-to-end encrypted and offers other security features like two-factor authentication, but is not open source and is a paid solution.

It offers team features and more, depending on the selected package. Options are listed in [this page](https://www.lastpass.com/business-password-manager).

- [LastPass technical specifications](https://lastpass.com/whylastpass_technology.php)
- [LastPass security page](https://lastpass.com/support.php?cmd=showfaq&amp;id=6926)


#### 1Password

[1Password](https://1password.com/) is a commercial paid online password manager. It is end-to-end encrypted with standard, open source encryption methods.

- [Security details](https://1password.com/security/)
- [1Password for Teams White Paper](https://1password.com/files/1Password%20for%20Teams%20White%20Paper.pdf)


* * *


### Comments
