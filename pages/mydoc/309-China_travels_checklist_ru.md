---
title: Setup Checklist for Clients Travelling to China - Russian
keywords: safer travels, travel security, email template, China
last_updated: February 6, 2019
tags: [organization_security_templates, templates]
summary: "Советы по безопасности для клиентов, которые отправляются в Китай со своими рабочими или личными устройствами"
sidebar: mydoc_sidebar
permalink: 309-China_travels_checklist_ru.html
folder: mydoc
conf: Public
ref: China_travels_checklist
lang: ru
---


# Setup Checklist for Clients Travelling to China
## Secure setup for clients who need to take their work or private devices to Mainland China

### Body

Здравствуйте,

Возвращаясь к разговору о ваших предстоящих путешествиях, я посылаю контрольный список рекомендаций. Он поможет вам еще до отъезда решить проблемы с цифровой безопасностью.

1. Создайте резервные копии всех важных данных с устройства, которое собираетесь взять с собой. Сохраните эти копии на зашифрованном внешнем носителе. Его вы оставите дома. Удалите оригинальные данные с устройства, которое собираетесь взять с собой. [ПРИ НЕОБХОДИМОСТИ ДОБАВИТЬ ИНСТРУКЦИИ ПО НАДЕЖНОМУ УДАЛЕНИЮ ДАННЫХ. ДЛЯ WINDOWS МОЖНО ССЫЛАТЬСЯ НА [ЭТО РУКОВОДСТВО](https://securityinabox.org/en/guide/destroy-sensitive-information/#wiping-information-with-secure-deletion-tools)]

    Если какие-либо данные нужны в поездке, загрузите зашифрованную папку с файлами в облачное хранилище данных. Приехав на место, можете скачать эти материалы, а затем удалить их снова перед возвращением домой. Если хотите, мы поможем подобрать надежный сервис для хранения файлов.

2. Включите полное дисковое шифрование. Используйте надежный пароль (парольную фразу).

    [РЕКОМЕНДАЦИИ ПО ПОЛНОМУ ДИСКОВОМУ ШИФРОВАНИЮ: [Article #166: Full-Disk Encryption (FDE)](166-Full-Disk_Encryption.html)]

3. Включите брандмауэр (в так называемом "скрытом режиме", означающем, что он не отвечает на пинги).

    [В ЗАВИСИМОСТИ ОТ ОПЕРАЦИОННОЙ СИСТЕМЫ КЛИЕНТА МОЖНО ПРЕДЛОЖИТЬ ТАКИЕ ССЫЛКИ:
    
    - [Windows Vista и 7](https://technet.microsoft.com/en-us/library/dd448557)
    - Для Windows мы можем также предложить [использовать Comodo вместо встроенного брандмауэра](https://help.comodo.com/topic-72-1-284-3059-.html)
    - [Для Mac](http://osxdaily.com/2015/11/18/enable-stealth-mode-mac-os-x-firewall/)
    - [Для Linux (GUFW)](https://www.everything-linux-101.com/how-to/firewalls-in-linux/)]

4. Убедитесь, что ваши устройства подключаются только к надежным сетям Wi-Fi, защищенным паролями. Подключения к открытым сетям Wi-Fi лучше избегать, но если подключиться необходимо, всегда используйте надежный VPN.

5. Включите защиту PIN-кодами или паролями для всех своих мобильных устройств. Когда не работаете с ними, отключайте (не отправляйте в "спящий режим"). По возможности берите устройство с собой. Если это невозможно, лучше оставить его в гостиничном сейфе, но имейте в виду, что сотрудники отелей могут открывать эти сейфы без вашего участия.

6. Отключите сервис определения вашего местонахождения.

7. Установите браузер Chrome и расширения для безопасной работы в интернете. 

    [Предложите клиенту оценить уровень его безопасности при работе в интернете в соответствии с этими материалами: [Article #212: Safe Browsing Practices and Plugins](212-Safe_Browsing_Practices.html).]

8. [ЕСЛИ КЛИЕНТУ НУЖЕН ВИДЕОЧАТ, А ТОТ НЕ РАБОТАЕТ ИЗ-ЗА ОГРАНИЧЕНИЙ КИТАЙСКОГО ПРАВИТЕЛЬСТВА НА ЗАРУБЕЖНЫЙ ТРАФИК] Для видеочата попробуйте [сервер Jitsi Meet от Calyx](https://meet.calyxinstitute.org)

9. [ЕСЛИ КЛИЕНТУ В ПОЕЗДКЕ НУЖЕН ДОСТУП К EMAIL] Установите Thunderbird с Enigmail и настройте на специально созданный временный почтовый адрес. Если работаете с Chromebook, можете воспользоваться расширением Mailvelope.

    9.1. Создайте пару ключей PGP для нового почтового аккаунта. [Можно ссылаться на [это руководство для Windows](https://guides.accessnow.org/pgp/PGP_Encrypted_Email_Windows.html) и [это руководство для Mac](https://guides.accessnow.org/pgp/PGP_Encrypted_Email_Mac.html)]

10. Установите и настройте мессенджеры со сквозным шифрованием, например, Signal, Wire и т.п.

    [В КИТАЕ ОЧЕНЬ ПОПУЛЯРЕН TELEGRAM, НО НАМ ДОПОДЛИННО ИЗВЕСТНО (НОЯБРЬ 2017 ГОДА), ЧТО ОН ЗАБЛОКИРОВАН, КАК И  WHATSAPP].


Пожалуйста, свяжитесь с нами, если у вас есть какие-либо сомнения или вопросы.

С уважением,

[ИМЯ СОТРУДНИКА]


* * *


### Related Articles

- [Article #248: Digital security advice when travelling to China](248-Travels_China.html)
- [Article #166: Full-Disk Encryption (FDE)](166-Full-Disk_Encryption.html)
- [Article #212: Safe Browsing Practices and Plugins](212-Safe_Browsing_Practices.html)
