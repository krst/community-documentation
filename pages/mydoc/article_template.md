---
title: Title of the article
keywords: include RT topic
last_updated: Month XX, 20XX
tags: [category, articles, (faq)]
summary: "Symptom of the article, within quotes as here"
sidebar: mydoc_sidebar
permalink: title_of_article.html
folder: mydoc
conf: Confidential
ref: unique_identifier_among_translations
lang: en
---
*leave this line blank*
*leave this line blank*
# Title of the article again
## Subtitle of the article

### Problem




* * *


### Solution




* * *


### Comments
