---
title: Come tradurre questo sito web
summary: "Breve guida per la localizzazione di questo sito"
sidebar: mydoc_sidebar
permalink: it_translation_readme.html
folder: mydoc
conf: Public
ref: translation_readme
lang: it
---


## Come tradurre una pagina

### Creare ed editare la pagina

Creare il file copiandolo da un'altra pagina e modificare nell'head in cima, dove bisogna aggiungere o compilare:

* ref:  
* lang:

Affinchè una pagina sia riconosciuta come traduzione di un'altra, deve avere lo stesso valore in "ref".

## Aggiungere un tag 

Verifica che esista il "template" di quel tag per quella lingua, altrimenti ti toccherà crearlo.

Puoi iniziare dal modello in inglese, per esempio:

	community-documentation/pages/tags/tag_browsing_security.md

E dobbiamo creare una copia di quel file, per esempio per l'italiano, e lo rinominiamo in questo modo:

	community-documentation/pages/tags/it_tag_browsing_security.md

e gli modifichiamo l'head così:

* lang:  it
* langNow: it

o con l'id della lingua prescelta.
Potresti avere da tradurre anche qualche frase, per esempio "The following pages and posts are tagged with:"
o la descrizione di quel tag, se esiste.

## Aggiungere una voce al menù laterale

Le voci e le sottovoci del menù laterale possono essere multilingua.
Per il primo livello si aggiunge "multilanguage" e al di sotto gli identificativi delle lingue con i titoli tradotti;
mentre nei livelli successivi, i folderitems, si lascia come titolo il titolo in inglese e i titoli tradotti sono aggiunti come figli di "m_title" ed ognuno con l'identificativo della lingua e il suo titolo tradotto.
Esempio:

<pre>
  - title: multilanguage
    en: Email Templates
    it: Modelli di Email
    ru: Шаблоны писем
    fl: Mga Gabay sa Email
    de: E-mail-Vorlagen
    fr: Modèles de email
    ar: نماذج الإيميل 
    es: Plantillas de correo
    pt: Templates de email
    output: web, pdf
    folderitems:
    
    - title: Account Recovery Templates
      m_title:
        ru: Шаблоны по восстановлению аккаунта
      url: /tag_account_recovery_templates.html
      output: web, pdf
      type: homepage
      lang: en, ru

</pre>

### Link a tag 

Bisogna modificare il file di configurazione del menu:

	community-documentation/_data/sidebars/mydoc_sidebar.yml

e aggiungere l'identificativo del linguaggio (it, fr, es, ...) alla lista delle lingue in cui quel tag o categoria o articolo esiste.
La composizione dell'url è fatta: id_nometag.html

### Titolo multilingua 


Bisogna modificare il file di configurazione del menu:

	community-documentation/_data/sidebars/mydoc_sidebar.yml

e aggiungere:

* il campo title deve essere "multilanguage", ovvero:  title: multilanguage
* l'identificativo del linguaggio (it, fr, es, ...) avrà come valore il titolo tradotto, esempio: it: Novità


## Aggiungere un link al menù nella barra in alto


Bisogna modificare il file di configurazione del menu:

	community-documentation/_data/topnav.yml

e aggiungere:

* il campo title deve essere multilanguage, ovvero:  title: multilanguage
* l'identificativo del linguaggio (it, fr, es, ...) avrà come valore il titolo tradotto, esempio: it: Novità

Anche i link possono essere multilingue. In questo caso si aggiunge multilanguage e poi si crea il figlio m_externalurl e quindi i figli sono gli identificativi delle lingue.

<pre>
        - title: multilanguage 
          en: Digital Security Helpline
          it: Helpline di sicurezza digitale
          external_url: multilanguage
          m_external_url: 
            en: https://accessnow.org/help
            it: https://accessnow.org/help
            es: https://www.accessnow.org/linea-de-ayuda-en-seguridad-digital/?ignorelocale
            fr: https://www.accessnow.org/plateforme-dassistance-pour-la-securite-numerique/?ignorelocale
            ru: https://www.accessnow.org/%d1%81%d0%bb%d1%83%d0%b6%d0%b1%d0%b0-%d0%bf%d0%be%d0%b4%d0%b4%d0%b5%d1%80%d0%b6%d0%ba%d0%b8-%d0%bf%d0%be-%d1%86%d0%b8%d1%84%d1%80%d0%be%d0%b2%d0%be%d0%b9-%d0%b1%d0%b5%d0%b7%d0%be%d0%bf%d0%b0%d1%81/?ignorelocale
            de: https://www.accessnow.org/help-deutsch/?ignorelocale
            pt: https://www.accessnow.org/linha-de-ajuda-em-seguranca-digital/?ignorelocale
            ar: https://www.accessnow.org/%d9%85%d8%b3%d8%a7%d8%b9%d8%af%d9%88-%d8%a7%d9%84%d8%a3%d9%85%d8%a7%d9%86-%d8%a7%d9%84%d8%b1%d9%82%d9%85%d9%8a/?ignorelocale
            fil: https://www.accessnow.org/helpline-tagalog/?ignorelocale
 
</pre>

## Aggiungere una nuova lingua di traduzione

Al momento le lingue contemplate sono 9, ma se si aggiungesse una lingua tutta nuova bisognerebbe metterla in questa lista:

	        var languages = [ar, de, en, es, fl, fr, it, pt, ru];

in questo file:

	community-documentation/_layouts/default.html	

ed anche aggiungere il suo stile qui, altrimenti non apparirebbe nel sito:

	community-documentation/css/customstyles.css

## Aggiungere una nuova homepage

Duplicare il modello della home che è nella root directory index.html, aggiungerli il suffisso della lingua es: it_index.html
Cambiare i campi: permalink, lang e title in quelli adatti per la lingua di destinazione.
Ricordarsi di aggiungere il titolo tradotto nel file _config.yml altrimenti finchè non lo traduci, non ti modifica secondo la lingua della pagina corrente il link home nel titolo del sitoweb.
