---
title: Initial Reply - For Non-French Speakers
keywords: email templates, initial reply, case handling policy
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-French speakers"
sidebar: mydoc_sidebar
permalink: 220-Initial_Reply_For_Non-French_Speakers.html
folder: mydoc
conf: Public
ref: Initial_reply_non-native
lang: fr
---


# Initial Reply - For Non-French Speakers
## First response, Email to Client if you're a non-French speaker

### Body


Che(è)r(e) [Client Name],

Mon nom est [Your Name], et je fais partie de l’équipe de réponse aux incidents numériques de AccessNow (https://www.accessnow.org/plateforme-dassistance-pour-la-securite-numerique)

J’ai bien reçu votre e-mail [Email Subject]. Malheureusement, je ne pourrai pas m’exprimer en Français. Si votre demande est urgente, veillez répondre à cet e-mail et ajouter le mot “URGENT” au sujet et nous essayerons de vous répondre dans les plus brefs délais.

Si vous pouvez vous exprimer en anglais, allez y. Vous pourriez me trouver en discussion instantanée sur [xxxx@accessnow.org] en anglais aussi.

Nos collègues qui parlent français seront dans leur bureaux dans quelques heures.

Merci,

[Your name]


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
