---
title: Recommendations on Secure File Sharing and File Storage
keywords: backup, file sharing, file storage, cloud, secure storage
last_updated: February 6, 2019
tags: [devices_data_security, articles]
summary: "A client needs a secure solution for sharing files and/or for storing them in the cloud"
sidebar: mydoc_sidebar
permalink: 282-Secure_file_sharing_storage.html
folder: mydoc
conf: Public
lang: en
---


# Recommendations on Secure File Sharing and File Storage
## Secure solutions for sharing files and for storing them in the cloud

### Problem

The client is sharing sensitive files with their colleagues and partners through insecure platforms that are neither end-to-end encrypted nor trusted.

The client is looking for a secure way of sharing or storing sensitive files.

The client might need to anonymize their connection when sharing a file.


* * *


### Solution

#### Questionnaire

In order to identify the best file sharing and file storage solutions for our client, we need to assess their needs and context, by answering the following questions:

##### Explore context and threats

- What are your main activities?
- What is your mission?
- In which country are you based?
- What is your main concern? *For example, you might be worried about your adversary accessing sensitive data, or about your personal identity being linked to specific data.*
- What kind of sensitive data do you handle and what could happen if there was a data breach?
- What would happen if your identity was connected to the shared files?
- Have you or some of your partners been attacked before? Which kind of attack?

##### Assess client's needs

- Do you need to store files permanently or to share files with others just once?
- Do you have your own server?
- Can you manage a server or do you have an IT person who could manage it for you?
- Do you have funds to rent a server/VPS?
- Do you already use a storage service? Which one?
- What service do you currently use to send large files?
- Do you need to share files with your team or also with people outside of your team?
- Do you need the service to be integrated with other features, like collaborative writing, calendar, etc.? Which features?
- What would you use the additional features for?
- Do you need access privileges for different kinds of files?
- Do you need to regroup the documents, for example by tagging them?
- Which devices are you going to use to access the files? PCs/laptops or mobile devices? Which operating systems?


#### Recommendations

Once we have assessed the client's needs and risks, we can suggest them the best options. We should always recommend more than one option, so that the client can decide what's the best for them.

We should also take into account the client's current workflow: if the client is already using a commercial service, moving to another file storage solution might disrupt their workflow, especially if they're using a full package of applications as G Suite. In such cases we should explore with them what their (and their organization's) capacities are and if they would rather stick to their current workflow or are willing to switch to a different solution. To help them make this decision, we should also consider if they have any adversaries that might have legal access to their commercial accounts.

If they don't have the capacities to change their workflow, we could work on securing their workflow, for example by suggesting they encrypt sensitive files with Veracrypt before they upload them to the cloud storage they are currently using, or by recommending that only people handling sensitive data use an alternative file sharing or storage service.

If the client is using a commercial service, like G Suite or Dropbox, we should instruct the client to check the security and sharing settings, and to enable multi-factor authentication wherever possible.

- **GDrive** - *See [Article #146: Secure File Sharing on Google Drive](146-Secure_File_Sharing_GDrive.html).*


- **Dropbox**
    - [Dropbox instructions on how to secure an account](https://www.dropbox.com/security#account)
    - [Dropbox instructions on how to set up 2-Step Verification](https://www.dropbox.com/help/security/enable-two-step-verification)
    - More recommendations for Dropbox:
        - Only add Dropbox to devices you need access to work documents on - each new device used with Dropbox is a new device you will need to protect.
    - Become familiar with the security settings and make adjustments accordingly, for instance:
        - Keep an eye on what extension apps you have allowed to view your Dropbox data. Remove apps that are no longer in use.
        - Receive notifications for when a new device has accessed the Dropbox account or a new app is added under the profile page.
        - Be aware that Dropbox protects your data in transit to Dropbox servers and to those you share it with, but it does not protect the data itself if someone were to enter your account or your recipients’ accounts, or otherwise access the documents.

If the client is willing and ready to switch to a different platform, we should base our recommendations on the client's needs and on the following criteria:

- Usability of the service
- Accessible end-user documentation
- Friendliness towards NGOs
- Customer service and responsiveness
- Costs
- Included services for groups, like a calendar or collaborative editing features
- Country where the company is based
- Security features (end-to-end encryption, HTTPS/TLS, 2-factor authentication...)
- Free and open source software both client- and server-side

What follows is a list of services that match with some or all of these criteria.
This list is by no means complete, but it includes a selection of cloud storage 
and file sharing solutions that offer good security features and are managed by 
trusted entities and/or located in safer jurisdictions.


##### Commercial

###### Packages

- [**GreenNet**](https://www.greennet.org.uk/internet-services/cloud-storage)
    - NGO-friendly
    - Paid service
    - Cloud file storage, which can be synchronized between as many computers and devices as you want
    - Full revision tracking of file modifications
    - Individual and shared calendars, task lists and shared contact lists, web-based document viewer, image viewer and music player
    - Access privileges for files and groups
    - Multiplatform sync (CalDAV and CardDAV protocols)
    - Can be combined with organizational email addresses and other [services provided by GreenNet](http://www.greennet.org.uk/internet-services)
    - Based in the UK
    - Free and open source software (Nextcloud)
    - HTTPS/TLS enabled
    - 100% green, renewable sources

- [**KolabNow**](https://kolabnow.com/feature/files)
    - Paid service
    - Server-side encryption
    - [Security features](https://kolabnow.com/feature/security)
    - [2-Factor authentication](https://kb.kolabnow.com/documentation/two-factor-authentication-2fa-at-kolab-now)
    - Full package like G Suite, including email, calendar, task manager, notes, collaborative writing
    - Access privileges for files and groups
    - Multi-platform sync
    - Based in Switzerland
    - Free and open source (Nextcloud + [Collabora Online](http://www.linux-magazine.com/Online/News/Kolab-Now-Integrates-Collabora-Online))

- [**SecureSafe**](https://www.securesafe.com)
    - [Freemium](https://www.securesafe.com/en/pricing/)
    - Client-side encryption
    - [Security features](https://www.securesafe.com/en/security/)
    - [2-Factor authentication](https://www.securesafe.com/en/faq/questions-about-login-and-account-management/how-can-i-activate-2-factor-authentication/)
    - Includes password manager, [file transfer](https://www.securesafe.com/en/news/transfer-files-online/), and [data inheritance](https://www.securesafe.com/en/news/data-inheritance-valuable-help-for-loved-ones/)
    - [Files can be assigned to teams](https://www.securesafe.com/en/news/secure-file-sharing/)
    - [Multi-platform sync](https://www.securesafe.com/en/sync/)
    - Proprietary
    - Based in Switzerland

- [**SpiderOak**](https://spideroak.com)
    - Paid service
    - End-to-end encrypted
    - [SpiderOak's declaration on security](https://spideroak.com/no-knowledge/)
    - [2-Factor authentication not available at the moment for new users](https://support.spideroak.com/hc/en-us/articles/115001894143-2-Factor-Authentication-for-Your-ONE-Account)
    - Multiplatform sync
    - Backup, group chat, file sharing, password manager
    - [Functionalities for groups](https://support.spideroak.com/hc/en-us/categories/115000416346-Groups-Backup)
    - Proprietary
    - Based in the United States


###### File storage only

- [**Sync.com**](https://www.sync.com)
    - Paid service
    - End-to-end encrypted
    - [Privacy Whitepaper](https://www.sync.com/pdf/sync-privacy.pdf)
    - [2-Factor authentication](https://www.sync.com/help/how-do-i-setup-two-factor-authentication/)
    - Multiplatform sync
    - Sharing, collaboration, and file transfer
    - Granular user permissions
    - Global data privacy compliance
    - Proprietary
    - Based in Canada

- [**Resilio Sync**](https://www.resilio.com/) (formerly BitTorrent Sync)
    - Commercial, paid app
    - Multiplatform peer-to-peer app
    - Not cloud-based - needs at least 5 more nodes to run
    - End-to-End Encryption - Resilio Sync encrypts data with an Advanced Encryption Standard AES-128 key in counter mode which may either be randomly generated or set by the user. This key is derived from a "secret" which can be shared to other users to share data.
    - Proprietary

- [**TeamDrive**](https://teamdrive.com/)
    - Paid service
    - End-to-end encrypted
    - Multiplatform sync
    - Granular user permissions
    - Automatic backup and version control
    - GDPR-compliant
    - Proprietary
    - Based in Germany

##### Free

###### Tools for groups

What follows is a list of software for groups, including file storage and sharing, that can be self-hosted. If the client wants to self-host one of these solutions and needs advice for choosing a **hosting provider**, refer to [Article #88: Advice on Hosting](88-Advice_Hosting.html).

- [**Nextcloud**](https://nextcloud.com/) - Nextcloud is a suite of client-server software for creating and using file hosting services.
    - Can be self-hosted - [Installation instructions](https://nextcloud.com/install/#instructions-server)
    - Hosted by several trusted entities, including KolabNow, GreenNet, and [disroot.org](https://disroot.org/en/services/nextcloud) - see all Nextcloud providers [here](https://nextcloud.com/providers/)
    - File storage, calendar, and [other optional apps](https://apps.nextcloud.com/)
    - Not all apps are functional (for example collaborative writing apps)
    - Possibility of enabling [end-to-end encryption](https://nextcloud.com/endtoend/)
    - Possibility of enabling [2-factor authentication](https://docs.nextcloud.com/server/12/user_manual/user_2fa.html)
    - Free and open source

- [**Sandstorm**](https://sandstorm.io/) - Sandstorm is an open source platform for self-hosting web apps
    - Can be used for file storage by installing [Davros](https://github.com/mnutt/davros/)
    - Can be used for file sharing by using [Filedrop](https://github.com/zombiezen/filedrop/)
    - Self-hosted - [Installation instructions](https://sandstorm.io/install)
    - [Features](https://sandstorm.io/features)
    - [Security practices](https://docs.sandstorm.io/en/latest/using/security-practices/)
    - Can be used on [Oasis Hosting](https://oasis.sandstorm.io/) ([Paid solution](https://sandstorm.io/get#oasis-pricing))
    - Free and open source

- [**Crabgrass**](https://0xacab.org/riseuplabs/crabgrass) - Crabgrass is a web application designed for activist groups to be better able to collaborate online.
    - Can be self-hosted - [Installation instructions](https://0xacab.org/riseuplabs/crabgrass/blob/master/doc/INSTALL.md)
    - Hosted by Riseup on [we.riseup.net](https://we.riseup.net/) - it doesn't need a Riseup mail account, but an account needs to be created
    - Not very intuitive
    - File sharing - gallery - video player - forum - public and private wiki - task manager - surveys - polls
    - Groups and subgroups can be created
    - Free and open source


###### File storage only

For secure file storage on any non-encrypted cloud service the client might be using, we can recommend them to encrypt files or folders with [**Veracrypt**](https://www.veracrypt.fr) before they upload them to the cloud, and to share the Veracrypt database channel with colleagues and partners through a separate encrypted channel.

- [Veracrypt Beginner's Tutorial](https://www.veracrypt.fr/en/Beginner%27s%20Tutorial.html)

Another solution can be using [**Cryptomator**](https://cryptomator.org) and syncing it with the used platform. Cryptomator is a free and open source multiplatform app that provides transparent, client-side encryption for cloud storage. To share a vault with others, the client will need to share its password through an encrypted channel.

- [Cryptomator Tutorial: Get Started](https://www.youtube.com/watch?time_continue=13&amp;v=g9A0zihHZ14)
- [Cryptomator Security architecture](https://cryptomator.org/security/architecture/)
- [Cryptomator Security advice](https://cryptomator.org/security/advice/)


###### File transfer

- [**Framadrop**](https://framadrop.org/)
    - Hosted by Framasoft - a French grassroots association that aims at providing secure and privacy-friendly alternatives to commercial online services
    - Client-side encryption
    - Free and open source ([Lufi](https://github.com/ldidry/lufi))
    - If needed, you can choose a delay of online availability
    - Does not require an add-on, and can be used in any modern browser

- [**share.riseup.net**](https://share.riseup.net/)
    - Hosted by Riseup
    - Client-side encryption
    - Free and open source ([Up1](https://github.com/Upload/Up1))
    - Upload is currently limited to 50mb and files are stored no longer than a week
    - Does not require an add-on, and can be used in any modern browser

- [**send.firefox.com**](https://send.firefox.com/)
    - Hosted by Mozilla
    - Client-side encryption
    - [Free and open source](https://github.com/mozilla/send)
    - Upload large files, up to 1Gb
    - Each link created by Send will expire after 1 download or 24 hours, and all sent files will be automatically deleted from the Send server
    - Does not require an add-on, and can be used in any modern browser


###### Anonymous file transfer

- [**Onionshare**](https://onionshare.org/) - OnionShare is an open source tool that lets you securely and anonymously share a file of any size.
    - App for Linux, Mac, and Windows
    - Tor Browser needs to be installed in the system to download the shared file
    - [Free and open source](https://github.com/micahflee/onionshare/wiki)
    - OnionShare has an advanced option, "Create Stealth Onion Service", that makes it impossible for an attacker to connect to the onion address created by OnionShare even if they learns the onion address. For more information see [this guide](https://github.com/micahflee/onionshare/wiki/Stealth-Onion-Services).


* * *


### Comments


* * *

### Related Articles

- [Article #88: Advice on Hosting](88-Advice_Hosting.html)
- [Article #146: Secure File Sharing on Google Drive](146-Secure_File_Sharing_GDrive.html)
