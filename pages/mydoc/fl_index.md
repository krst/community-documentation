---
title: "Kasulatan ng  Komunidad ng Digital Security Helpline"
keywords: home
tags: [home]
sidebar: mydoc_sidebar
permalink: fl_index.html
ref: home
lang: fl
---


# Kasulatan ng Komunidad ng Digital Security Helpline

Sa website na ito makikita mo ang mga gabay na ginagamit namin sa Access Now Digital Security Helpline upang mahawakan ang iba't ibang uri ng mga kaso sa panahon ng aming 24/7 na operasyon.

Kung nagpapatakbo ka ng digital helpdesk ng seguridad o nagpapayo lamang sa iyong mga kaibigan kung paano pagtibayin ang seguridad ng kanilang mga online na aktibidad o tumugon sa isang insidenteng pang-seguridad sa digital, maaari kang makahanap ng ilang mga kapaki-pakinabang na pamamaraan sa mga gabay na ito.

Kung nais mong i-update ang isang pahina, maaari mong pindutin ang pindutan ng pag-edit o bisitahin ang [Gitlab repository](https://gitlab.com/AccessNowHelpline/community-documentation).

{% include links.html %}


