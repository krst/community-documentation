---
title: Training Resources
keywords: trainings, digital security trainings, workshops, resources, ADIDS
last_updated: December 18, 2018
tags: [direct_intervention, articles]
summary: "We have been asked to organize a digital security training and need resources to prepare it."
sidebar: mydoc_sidebar
permalink: 301-Training_Resources.html
folder: mydoc
conf: Public
lang: en
---


# Training Resources
## A list of resources for digital security trainings

### Problem

An organization or group of activists has requested a training and we need resources to prepare it.


* * *


### Solution

Digital Security Trainings run by the Helpline should be based on [ADIDS, an adult-focused approach to learning](https://level-up.cc/before-an-event/levelups-approach-to-adult-learning/).

- [How-To: Preparing Sessions Using ADIDS](https://level-up.cc/before-an-event/preparing-sessions-using-adids/)
- [How to Teach Adults](https://sec.eff.org/articles/how-to-teach-adults)
- [A training of trainers module on adult learning and ADIDS](https://www.fabriders.net/tot-adids/)

To get ready for a training, you can follow these guides:

- [Before an Event](https://level-up.cc/before-an-event/)
- "Security 101" section in the [Security Education Companion](https://sec.eff.org/articles)
- "Best Practices" chapter in the [Holistic Security Trainers' Manual](https://holistic-security.tacticaltech.org/ckeditor_assets/attachments/60/holisticsecurity_trainersmanual.pdf)


You will find activities and curricula that you can use for inspiration in these resources:

- [Level Up - Resources for the global digital safety training community](http://level-up.cc/)
- [EFF's Security Education Companion](https://sec.eff.org)
- [Me and My Shadow Training Curriculum](https://myshadow.org/train)
- [Digital Security Training Resources for Security Trainers, Winter 2017 Edition](https://medium.com/cryptofriends/digital-security-training-resources-for-security-trainers-spring-2017-edition-e95d9e50065e)
- [Gendersec Training Curricula](https://tacticaltech.org/projects/gendersec-training-curricula/) - with activities for women and gender minorities


* * *


### Comments



* * *


### Related Articles
