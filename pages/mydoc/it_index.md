---
title: "Documentazione pubblica della Digital Security Helpline di Access Now"
keywords: home
tags: [home]
sidebar: mydoc_sidebar
permalink: it_index.html
ref: home
lang: it
---


# Documentazione pubblica della Digital Security Helpline di Access Now

Questo sito contiene le guide che usiamo alla Digital Security Helpline di Access Now per affrontare i vari tipi di casi con cui abbiamo a che fare durante il nostro lavoro, che si svolge 24 ore su 24 tutti i giorni dell'anno.

Se lavori in uno sportello d'aiuto per la sicurezza digitale o stai semplicemente aiutando un'amica o un amico a mettere in sicurezza le sue attività online o a risolvere un incidente di sicurezza digitale, potrai trovare consigli e procedure utili in queste guide. 

Se vuoi aggiornare una pagina, clicca sul tasto "Edit me" o visita questa [repository](https://gitlab.com/AccessNowHelpline/community-documentation) su Gitlab.

{% include links.html %}


