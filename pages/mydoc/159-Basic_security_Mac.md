---
title: Basis Security Measures for Mac Computers
keywords: Mac, basic security, Filevault, software security, firewall
last_updated: August 23, 2018
tags: [devices_data_security, articles]
summary: "A client has a new Mac and they are asking for basic digital security tools and practices for their device."
sidebar: mydoc_sidebar
permalink: 159-Basic_security_Mac.html
folder: mydoc
conf: Public
lang: en
---


# Basis Security Measures for Mac Computers
## Advice about basic digital security measures to take on Mac

### Problem

High-risk users might not be protected sufficiently by the default security settings of Mac computers. We need to check that basic security settings are set up correctly and that the client is also using the safest software available.


* * *


### Solution

#### Initial assessment

We need to ask the client about their job and security context.

1. What do they use the computer for? 
2. Do they travel with it?
3. Do they have any specific needs?

If the client's threat model needs to be assessed more thoroughly, also see the questions in [Article #200: Lightweight Security Assessment](200-Lightweight_Security_Assessment.html).

#### Basic Recommendations

These are some basic digital security best practices:

1. Hard drive encryption: check that FileVault is enabled, and enable it if it's not.

    In recent Mac versions, FileVault is enabled by default. It is worth checking in any case that it is enabled in the client's computer. For more information, see [Article #104: Full-Disk Encryption on Mac with FileVault 2](104-FDE_Mac_FileVault2.html).

    - Instructions for enabling FileVault can be found [here](https://support.apple.com/en-us/HT204837).
    - More information on FileVault and full disk encryption can be found in Citizen Lab's [Security Planner](https://securityplanner.org/#/tool/mac-encryption).

2. Enable Mac firewall.

    - For an explanation of what a firewall does and why it's important to enable it, we can send [this link](https://securityplanner.org/#/tool/mac-firewall) of the Security Planner to the client.
    - Instructions on how to enable Mac firewall can be found [here](https://support.apple.com/en-au/HT201642). 

3. Make sure that the system automatic update is enabled.

    - Send the client [this link with instructions to update their software](https://support.apple.com/en-us/HT201541).
    - Send the client [this link with instruction on how to enable automatic software updates](https://support.apple.com/kb/PH25532?locale=en_US&amp;viewlocale=en_US)
    - More information on why automatic software updates are important can be found in [this page](https://securityplanner.org/#/tool/keep-your-mac-updated).

4. Install an antivirus, and consider installing an antimalware.

    For more instructions on antivirus tools for Mac, see [Article #128: Antivirus for Mac](128-Antivirus_for_Mac.html)

4. Suggest the client to use Chromium or Firefox as default browser and to install addons for privacy and security.

    For more instructions on browsing security, refer to [Article #212: Safe Browsing Practices and Plugins](212-Safe_Browsing_Practices.html).

5. Change the default search engine to DuckDuckGo.

6. Consider sending recommendations on basic security hygiene and on other security tools, such as password managers and VPNs.


* * *


### Comments


* * *


### Related Articles

- [Article #200: Lightweight Security Assessment](200-Lightweight_Security_Assessment.html)
- [Article #104: Full-Disk Encryption on Mac with FileVault 2](104-FDE_Mac_FileVault2.html)
- [Article #128: Antivirus for Mac](128-Antivirus_for_Mac.html)
- [Article #212: Safe Browsing Practices and Plugins](212-Safe_Browsing_Practices.html)
-
