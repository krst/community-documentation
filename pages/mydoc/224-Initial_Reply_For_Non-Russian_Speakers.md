---
title: Initial Reply - For Non-Russian Speakers
keywords: email templates, initial reply, case handling policy
last_updated: February 7, 2019
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client if you're a non-Russian speaker"
sidebar: mydoc_sidebar
permalink: 224-Initial_Reply_For_Non-Russian_Speakers.html
folder: mydoc
conf: Public
ref: Initial_reply_non-native
lang: ru
---


# Initial Reply - For Non-Russian Speakers
## First response, Email to Client if you're a non-Russian speaker

### Body


Здравствуйте,

Меня зовут [YOUR NAME]. Я работаю в Службе поддержки по вопросам цифровой безопасности организации Access Now (https://www.accessnow.org/help).

Ваш запрос ([EMAIL SUBJECT]) получен. К сожалению, я не говорю по-русски. Если вопрос срочный, пожалуйста ответьте на это письмо, добавив слово "URGENT" в поле темы письма. Мы постараемся обработать ваш запрос как можно скорее. 

Если вам не составит труда продолжить переписку на английском языке, пожалуйста, сообщите. Связаться со мной можно по адресу [YOUR EMAIL]. 

Наш русскоговорящий сотрудник будет в офисе в течение нескольких часов и ответит на ваше сообщение.

С уважением,

[YOUR NAME]


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
