---
title: Inform Clients Targeted by Harassment about Vetting Process - Russian
keywords: email templates, vetting, vetting process, harassment, gendered online violence
last_updated: February 7, 2019
tags: [helpline_procedures_templates, harassment_templates, templates]
summary: "Шаблон для информирования клиентов, подвергшихся онлайновой агрессии по гендерному признаку, о нашей процедуре проверки"
sidebar: mydoc_sidebar
permalink: 337-Vetting_Info_Clients_Harassment_ru.html
folder: mydoc
conf: Public
ref: Vetting_Info_Clients_Harassment
lang: ru
---


# Inform Clients Targeted by Harassment about Vetting Process
## Template to inform clients who have been targeted by gendered online violence about our vetting process

### Body

Здравствуйте,

Спасибо, что доверяете нашей службе поддержки. Надеемся, сейчас у вас все в порядке. Для защиты наших пользователей и расширения сети доверия мы хотели бы побольше узнать о вас и получить сведения о вас от партнеров Access Now.

Мы передадим по защищенным каналам связи ваши имя и электронный адрес, чтобы подтвердить вашу личность и удостовериться, что вы действительно гражданский активист. Вся прочая информация, включая причину обращения в нашу службу поддержки, останется строго конфиденциальной.

Чтобы подтвердить вашу личность, мы планируем обратиться в следующие организации: [НАЗВАНИЕ ОРГАНИЗАЦИИ, НО НЕ ФАМИЛИЯ СОТРУДНИКА!] и [НАЗВАНИЕ ОРГАНИЗАЦИИ, НО НЕ ФАМИЛИЯ СОТРУДНИКА!]. Если у вас есть причины считать, что это не самый удачный выбор в вашем случае, пожалуйста, сообщите нам.

Пожалуйста, также обратите внимание, что служба поддержки Access Now не предназначена для лиц младше 18 лет. Если вам еще нет 18 лет, просим сообщить нам об этом. Мы свяжем вас с партнерами, которые смогут оказать вам необходимую поддержку.

Процедура проверки будет запущена через 48 часов, если мы не получим от вас возражений. Во время этой процедуры наша команда продолжит работать с вашим запросом.

С уважением,

[ИМЯ СОТРУДНИКА]


* * *


### Comments

Please remember not to mention the name of the individual vettor we are going to reach out to.
