---
title: Data Acquisition Using Android Debug Bridge (ADB)
keywords: mobile, forensics, malware, dead image acquisition
last_updated: January 30, 2019
tags: [forensics, articles]
summary: "The Helpline has come into possession of an Android device that could be infected or can be used as digital evidence."
sidebar: mydoc_sidebar
permalink: 305-Data_acquisition_Android_Debug_Bridge.html
folder: mydoc
conf: Public
lang: en
---

# Data Acquisition Using Android Debug Bridge (ADB) 
## How to perform dead image acquisition on an Android device

### Problem

Physical data acquisition of a mobile device is requested because the device seems to be infected with malware or needs to be used as digital evidence.


* * *


### Solution

#### Requirements

1. A forensic distribution with Android Debug Bridge enabled and dc3dd installed for performing a byte-copy.

    Android Debug Bridge (ADB) is a command line tool which enables communication and supports a number of interactions with an Android device connected via USB.

2. Read [Article #252: Forensic Handling of Data](252-Forensic_Handling_Data.html) before following this procedure.

     The following procedure is a quick tutorial focused on 4 tools (DEFT/SANTUKU,  dc3dd, netcat, and ADB), but the recommendations detailed in Article #252 should always be followed to avoid any alteration of the evidence collected.


#### Procedure

For the command examples, these name conventions will be used:

* * *

| **Command format**                     | **Example**    | **Description** |
|----------------------------------------|----------------|-----------------|
| [FileName].[extension]                 | DeviceImage.dd | The name of the image file to be created  |
| /dev/PATH/[Device physical disk label] | /dev/block/sdW | Label of physical disk device (source).   |

* * *

*Note:* The path for the source may vary, it could be `/block/`.

1. Boot [DEFT](www.deftlinux.net/)/[SANTOKU](https://santoku-linux.com/about-santoku/) (or another forensic distribution with ADB installed) in the forensic workstation and open two terminals in order to have two different sessions: one shell session to the device, and one shell session to the forensic workstation.

2. Check that USB Debugging mode is enabled in the device. If it is enabled, connect the forensic workstation to the device using a USB cable and wait for the debugging icon to appear in the Android device.

    *If USB Debugging mode is not enabled, you should look for the procedure to enable it in the specific device model you are working with.*

    The `netcat` command will be used to forward commands across ports, read the device block representing the entire device and write it to the target directory in the forensic workstation over the USB connection (see below for details).

3. Using the shell session to the forensic workstation:

    - Create a new directory (in this example it will be named `InfoDevice`) in the forensic workstation to save all the logs and the byte-copy image:

        * * *

        | **Command format** |  `mkdir [DirectoryName]` |
        |--------------------|--------------------------|
        | **Example**        |  *`mkdir InfoDevice`*    |

        * * *

    - Navigate to the new directory and open the remote ADB shell:

            cd InfoDevice

            adb shell

    - Enable ADB to communicate via `netcat` on *port 8888* (another port can be selected, such as 4444). 

            adb forward tcp:8888 tcp:8888


4. Using the shell session to the device:

    - Open the remote ADB shell:

            adb shell

    - Change to super user

            su

    - Check if the device is connected by listing the devices attached 
  
            adb device

    - Check the device file system and search where the **`/data`** partition is located (for example: `/dev/block/[Device physical disk label]`)

            mount

    - Check the available disk space on the partition (it will show the total space, as well as the used and available space).

            df

    - Using the shell session to the device, generate the device hash and save it in a file in the destination media (in this example the file name is `SDCardOriginalHASH.md5`):

        * * *
        
        | **Command format** |  [checksumUsed] /dev/PATH/[SD Card physical disk label] \| busybox nc -l -p [TCPPortSelected] |
        |--------------------|---------------------------------------------------------------------------------------------|
        | **Example**        | *md5sum /dev/block/sdW \| busybox nc -l -p 8888* |

        * * *

    - In the shell session to the forensic workstation, save the output of the contents across port 8888 to the file `DeviceHASH.md5`

        * * *
        
        | **Command format** |  nc localhost [TCPPortSelected] &gt; [FileNameWheretheHashWillBeSaved].[checksumUsed] |
        |--------------------|-----------------------------------------------------------------------------------|
        | **Example**        | *nc localhost 8888 &gt;  DeviceHASH.md5* |

        * * *

5. Byte-Copy
 
    - Using the shell session to the device, perform the `dc3dd` command:

        * * *
        
        | **Command format** | dc3dd if=/dev//PATH/[Device physical disk label]  hash=[checksumUsed] log=logs.txt \| busybox nc -l -p [TCPPortSelected] 	|
        |--------------------|--------------------------------------------------------------------------------------------------------------------------	|
        | **Example**        | *dc3dd if=/dev/block/sdW hash=md5 log=logs.txt \| busybox nc -l -p 8888* |

        * * *

        **NOTE:** Another checksum that can be used is SHA256 (first check that the checksum utility is available in the distribution you are using) - just replace md5sum with sha256sum (or other checksum) and change the file extension (.md5 to .sha256) in the commands above.

    - Check the results displayed in the terminal (search for error messages, etc.).

    - In the shell session to the forensic workstation, save the output of the contents across port 8888 to the file `DeviceImage.dd`.

        * * *
        
        | **Command format** |  nc localhost [TCPPortSelected] &gt; [DeviceImage].dd    |
        |--------------------|-------------------------------------------------------|
        | **Example**        | *nc localhost 8888 &gt; DeviceImage.dd*                  |

        * * *
        
6. Once the imaging process is over, in the shell session to the forensic workstation generate a hash of the image file you just created and save it in a file in the workstation (in this example named `ByteCopyHASH.md5`).

    * * *
        
    | **Command format** 	| [checksumUsed] /[DirectoryName]/[DeviceImage].dd &gt; [FileNameWheretheHashWillBeSaved].[checksumUsed] |
    |--------------------|----------------------------------------------------------------------------------------------------|
    | **Example**        	| *md5sum /DeviceInfo/DeviceImage.dd &gt; ByteCopyHASH.md5* |

    * * *
        
    - Make sure the files are identical by comparing the two hashes with the following command:

        * * *
        
        | **Command format** | cat *.[checksumUsed]|
        |--------------------|---------------------|
        | **Example**        | _cat *.md5_           |

        * * *
        
        This command will display the two hashes one over the other. If the hashes are the same, you have generated an exact copy. If the hashes are different, the source disk and target image are not identical and you should repeat the process.

7. Make a list of all the installed packages and save it in a file.

    - In the shell to the device:
    
            adb ls /data/data | busybox nc -l -p 8888
 
    - In the shell to the forensic workstation:
    
            nc localhost 8888 &gt; /DeviceInfo/packages.txt.

8. Back up all apps:

    - In the shell to the device:

        * * *
        
        | **Command format** | adb backup -all -f  \| busybox nc -l -p [TCPPortSelected] 	|
        |--------------------|-------------------------------------------------------- -----|
        | **Example**        | *adb backup -all -f  \| busybox nc -l -p 8888*            	|

        * * *
        
     **NOTE:** unlock the Android device and select “Backup up my data”. A password can be optionally set.

    - In the shell to the forensic workstation:

        * * *
        
        | **Command format** | nc localhost [TCPPortSelected] &gt;  /[DirectoryName]/[BackupName].ab |
        |--------------------|--------------------------------------------------------------------|
        | **Example**        | *nc localhost 8888 &gt; /DeviceInfo/backup.ab* |

        * * *
        
9. Dump the live log of all processes running on the device in a file:

    - In the shell to the device:

        * * *
        
        | **Command format** | adb logcat -d \| busybox nc -l -p  [TCPPortSelected] |
        |--------------------|----------------------------------------------------|
        | **Example**        | *adb logcat -d \| busybox nc -l -p 8888*           |

        * * *
        
    - In the shell to the forensic workstation:

        * * *
        
        | **Command format** | nc localhost [TCPPortSelected] &gt; /[DirectoryName]/[LogFileName].txt |
        |--------------------|----------------------------------------------------------------------|
        | **Example**        | *nc localhost 8888 &gt; /InfoDeviceTICKET16233/logcat.txt*              |

        * * *
        
10. In both terminals, exit the adb shell:

        exit

11. Copy all the commands entered and create a file with this information:

        cat &gt; commands.txt

    (paste the information and escape with `ctrl D`)

12. Check the files listed in the destination media/final storage directory:

        ls

13. Get the device image using another tool. In order to double-check the image and the data collected, create another image using another tool, for example, [Magnet](https://www.magnetforensics.com), so the forensic analyst can test and analyze both images and avoid contradictions or errors. Magnet toolbox is quite easy to use, just follow these [instructions](https://www.magnetforensics.com/mobile-forensics/image-smartphone-magnet-acquire/).

    **NOTE:** Select the full extraction option.

14. Add this information to the encrypted external medium or container created for the case. Remember that during this data acquisition process sensitive data can be found in the byte-copy image and should be handled carefully.


* * *


### Comments

#### References

- [No Tool Fits All – Why Building a Solid Toolbox Matters](http://cyberforensicator.com/2018/07/15/no-tool-fits-all-why-building-a-solid-toolbox-matters/)
- [Estado  del  arte  de  las  técnicas  digitales  forenses  para 
dispositivos Android](https://upcommons.upc.edu/bitstream/handle/2117/101236/memoria.pdf) [Dissertation]
- [How to perform a logical acquisition using Santoku Linux using adb?](https://infosecaddicts.com/logical-acquisition-using-santoku-adb/)
- [Introduction to Android debugging using adb and busybox - adb backup/restore](https://gnu-linux.org/wp-content/uploads/introduction-to-android-debugging/introduction-to-android-debugging.html#adb-backup-restore)
- [How to Perform Physical Acquisition in Android Forensics?](https://infosecaddicts.com/perform-physical-acquisition-android-forensics/)
- [Physical Imaging Of A Samsung Galaxy S7 Smartphone Running Android 7.0](https://articles.forensicfocus.com/2017/08/08/physical-imaging-of-a-samsung-galaxy-s7-smartphone-running-android-7-0/)
- [Physical acquisition of a locked android device](https://www.digitalforensics.com/blog/physical-acquisition-of-a-locked-android-device/)


* * *


### Related Articles

- [Article #252: Forensic Handling of Data](252-Forensic_Handling_Data.html)
