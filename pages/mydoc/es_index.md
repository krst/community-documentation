---
title: "Documentación para la comunidad de la línea de ayuda en seguridad digital"
keywords: home
tags: [home]
sidebar: mydoc_sidebar
permalink: es_index.html
ref: home
lang: es
---


# Documentación para la comunidad de la línea de ayuda en seguridad digital

En este sitio encontrará las guías utilizadas por la  línea de ayuda en seguridad digital de Access Now para el manejo de incidentes.

Si usted opera una línea de ayuda en seguridad digital o simplemente brinda asesoría a sus amigxs sobre cómo asegurar sus actividades en línea o responder a incidentes digitales, estas guías le serán de utilidad.

Si desea actualizar alguna página, por favor de click en el botón de editar o puede visitar directamente este [repositorio de Gitlab](https://gitlab.com/AccessNowHelpline/community-documentation).

{% include links.html %}


