---
title: Title of the template
keywords: include RT topic
last_updated: Month XX, 20XX
tags: [category, templates]
summary: "Summary of the template, within quotes as here"
sidebar: mydoc_sidebar
permalink: title_of_article.html
folder: mydoc
conf: Confidential
ref: unique_identifier_among_translations
lang: en
---


# Title
## Subtitle

### Body

Dear xxxxx,
