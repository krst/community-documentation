---
title: "Documentação da comunidade da Linha de Ajuda em Segurança Digital"
keywords: home
tags: [home]
sidebar: mydoc_sidebar
permalink: pt_index.html
ref: home
lang: pt
---


# Documentação da comunidade da Linha de Ajuda em Segurança Digital

Neste site, você encontrará guias que usamos na linha de ajuda em segurança digital do Access Now Digital para lidar com diferentes tipos de casos durante nossas operações 24 horas por dia, 7 dias por semana.

Se você administra um helpdesk de segurança digital ou está apenas aconselhando seus amigos sobre como proteger suas atividades on-line ou responder a um incidente de segurança digital, você pode encontrar algumas dicas e procedimentos úteis nesses guias.

Se você quiser atualizar uma página, basta clicar no botão de edição ou visitar [este repositório do Gitlab](https://gitlab.com/AccessNowHelpline/community-documentation).

{% include links.html %}


