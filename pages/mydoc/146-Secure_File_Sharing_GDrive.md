---
title: Secure File Sharing on Google Drive
keywords: backup, file sharing, file storage, cloud, secure storage, GDrive, Google Drive
last_updated: December 14, 2018
tags: [devices_data_security, articles]
summary: "A client uses Google Drive for sharing sensitive documents and needs to review their security practices."
sidebar: mydoc_sidebar
permalink: 146-Secure_File_Sharing_GDrive.html
folder: mydoc
conf: Public
lang: en
---


# Recommendations on Secure File Sharing and File Storage
## A list of security recommendations and practices for sharing documents in Google Drive

### Problem

Google Drive is one of the most widespread platforms for storing and sharing documents. Using Google Drive for online data storage can offer many benefits to organizations, but there are also challenges in keeping data secure and private in the cloud.


* * *


### Solution

*Before you send recommendations on securing their GDrive account and practices, a general review of their needs and threat model may be useful, also to make sure that Google Drive is the best solution for them. Please refer to the questionnaire and recommendations in [Article #282: Recommendations on Secure File Sharing and File Storage](282-Secure_file_sharing_storage.html).*

1. Make sure the Google account is secured
    - Send the client [Google's instructions on how to secure an account](https://support.google.com/accounts/answer/46526)
    -  The first step should be a security checkup, to make sure that the account is safe.
        -  To do the security checkup, the client can start [here](https://security.google.com/settings/security/secureaccount)
    
    - Implementing 2-step verification is highly recommended.
        - To enable 2-step verification, the client can start [here](https://www.google.com/landing/2step/)
        - [Google instructions on how to set up 2-Step Verification](https://support.google.com/accounts/answer/185839)

2. Send the following recommendations to the client:
    - Don't install Google Drive on a public or shared device. Doing so may allow anyone with access to that device to be able to open or view your files.
    - Lock down permission of shared documents on Google Drive
        - [Google Drive instructions on how to share and unshare files](https://support.google.com/drive/answer/2494893)
        - Choose an appropriate sharing setting: Private, Anyone with the link, or Public
        - Explore advanced options for Link sharing. When you share a file or a folder, you can click "Advanced" in the sharing window. When you click "Specific people can access", you'll be presented further options like if you want to only share the link within your organization.
        - Try to avoid sharing sensitive files in Google Drive with "Anyone With Link" because anyone outside of your organization who finds the link would be able to access the document, posing a security risk given the sensitive information that might be contained in that file.
        - Also note that the amount of time in which someone can access a document can be limited and that you can allow someone to access that file for only a day, a month, or a year.

    - Be careful deleting user accounts of document owners
        - Note that Google documents created by a particular user reside in their account. Deleting a particular user account (e.g. after resignation) deletes the docs they created as well, regardless of whether or not those docs are shared with others. To avoid loss of data, transfer the ownership of important docs before deleting the user account. To transfer ownership, please read this guide: [https://support.google.com/a/answer/1247799](https://support.google.com/a/answer/1247799).
    - Create a secure backup of sensitive documents.

3. For highly confidential documents, consider recommending file encryption before uploading to Google Drive.
    - [VeraCrypt](https://www.veracrypt.fr), [GPG tools](https://gpgtools.org/), and [AES Crypt](https://www.aescrypt.com/) are sample tools that you can consider. depending on the client's operating system and needs.
    - For Mac users, also see [Article #76: Encrypt files on a Mac with GpgTools](76-Encrypt_files_Mac_GpgTools.html).


* * *


### Comments


* * *

### Related Articles

- [Article #282: Recommendations on Secure File Sharing and File Storage](282-Secure_file_sharing_storage.html)
- [Article #76: Encrypt files on a Mac with GpgTools](76-Encrypt_files_Mac_GpgTools.html)
