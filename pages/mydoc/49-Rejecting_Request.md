---
title: Rejecting a Request
keywords: case handling policy, out of mandate
last_updated: November 08, 2018
tags: [helpline_procedures, articles]
summary: "A person who has reached out to the Helpline is not a member of civil society, or during the vetting process we could not verify their email. Their email address belongs to a free email provider like Gmail or Outlook, or the user name contains numbers. The requestor has tried to prove their identity with irrelevant attachments (ID, etc.), or their message is incomprehensible despite our best efforts to ask for explanations."
sidebar: mydoc_sidebar
permalink: file_name.html
folder: mydoc
conf: Public
lang: en
---


# Rejecting a Request
## A person who has reached out to the Helpline cannot benefit from our help and we need to inform them

### Problem

A requestor is not part of the groups included in the Helpline's mandate:

* We support actors for change and civil society, including independent or citizen journalists, bloggers, human rights defenders, activists, opposition political parties, independent media outlets, human rights lawyers, Non-Governmental Organizations (NGOs), and other not-for-profit organizations. (See the [document on our constituency](https://git.accessnow.org/access-now-helpline/helpline-csirt-documentation/blob/master/1-General_Items/1-1-Defined_constituency.md) for more details).

* These individuals can be verified through a website, via social networks, or have established reputations on the web.

* The email is not considered spam or a scam after analysis.

* The client is far from being verifiable through the vetting process.


* * *


### Solution

Reply to the client explaining briefly and concisely the role we play and the client niche we are targeting.

The reply may potentially ask how our email address
<help@accessnow.org>
 was brought to their attention.

You can base your reply on the template in [Article #8: Out of Mandate](8-Out_of_Mandate.html).


* * *


### Comments



* * *

### Related Articles

- [Article #8: Out of Mandate](8-Out_of_Mandate.html)
</help@accessnow.org>