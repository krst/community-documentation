---
title: Post-DSC Outreach Template - Russian
keywords: email templates, outreach, events, DSC, digital security clinics
last_updated: February 7, 2019
tags: [helpline_procedures_templates, templates]
summary: "Шаблон электронного письма для продвижения; адресаты - гражданские активисты, с которыми мы встречались во время клиники по цифровой безопасности или на каком-либо мероприятии"
sidebar: mydoc_sidebar
permalink: 331-Post-DSC_Outreach_Template_ru.html
folder: mydoc
conf: Public
ref: Post-DSC_Outreach_Template
lang: ru
---


# Post-DSC Outreach Template
## Email template for outreach - addressed at members of civil society we met during a Digital Security Clinic or an event

### Body

Здравствуйте,

Это [ИМЯ СОТРУДНИКА] из Службы поддержки по вопросам цифровой безопасности организации Access Now. Мы встречались на мероприятии [НАЗВАНИЕ ВСТРЕЧИ DSC/Event].

Мне очень понравилось общение с вами. Наша служба поддержки доступна 24 часа в сутки круглый год. Мы оказываем помощь в области цифровой безопасности и по другим интересующим вас техническим вопросам. Подробнее о нашей службе можно прочитать здесь: https://www.accessnow.org/help. Мы всегда будем рады вашему письму: help@accessnow.org.

Прикладываю свой открытый ключ PGP. Если вы не используете PGP, мы можем помочь вам с настройкой программы или посоветовать другое средство для безопасного общения. Всегда хорошо иметь под рукой надежный канал связи, может пригодиться в будущем.

С удовольствием помогу и отвечу на ваши вопросы.

С уважением,

[ИМЯ СОТРУДНИКА]
